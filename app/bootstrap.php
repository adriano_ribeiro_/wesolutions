<?php

require "../vendor/autoload.php";

require "../config/configuration.php";

\app\utils\Bind::bind('connection', \app\database\Connection::connect());