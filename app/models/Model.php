<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 27/05/2018
 * Time: 00:20
 */
namespace app\models;

use app\traits\Crud;
use app\utils\Bind;
use PDO;
use PDOException;
use Throwable;


abstract class Model
{
    use Crud;
    protected $connection;

    public function __construct()
    {
        try {
            $this->connection = Bind::get('connection');
        } catch (PDOException $e) {
            die('Erro na conexão: ' . $e->getMessage());
        }
    }

    public function findAll()
    {
        try {
            $this->sql = "SELECT * FROM {$this->table}";
            $this->crud = $this->connection->query($this->sql);
            $this->crud->execute();

            return $this->crud->fetchAll(PDO::FETCH_OBJ);

        } catch (Throwable $t) {
            $t->getMessage();
        }
    }

    public function findById($id)
    {
        try {
            $this->sql = "SELECT * FROM {$this->table} WHERE {$id} = :{$id}";
            $this->crud = $this->connection->prepare($this->sql);
            $this->crud->bindValue($id, PDO::PARAM_INT);
            $this->crud->execute();

            return $this->crud->fetch(PDO::FETCH_OBJ);

        } catch (Throwable $t) {
            $t->getMessage();
        }
    }

    public function delete($id, $value)
    {
        try {
            $this->sql = "DELETE FROM {$this->table} WHERE {$id} = :{$id}";
            $this->crud = $this->connection->prepare($this->sql);

            return $this->crud->execute([$id => $value]);

        } catch (Throwable $t) {
            $t->getMessage();
        }
    }
}