<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 27/05/2018
 * Time: 00:20
 */

namespace app\models;


use PDO;
use PDOException;

class User extends Model
{
    public $table = 'wesolutions.users';

    public function login()
    {
        try{
            $stm = $this->connection->prepare("SELECT * FROM {$this->table} WHERE email = :email AND password = :password");
            $stm->bindParam(':email', $_POST['email'], PDO::PARAM_STR);
            $stm->bindParam(':password', $_POST['password'], PDO::PARAM_STR);
            $stm->execute();

            if ($linha = $stm->fetch(PDO::FETCH_ASSOC)):
                return $linha;
            else:
                echo "<script> alert('email ou senha inválidos'); </script>";
            endif;
        } catch (PDOException $e){
            $e->getMessage();
        }
    }
}