<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 27/05/2018
 * Time: 00:26
 */

namespace app\utils;


use Exception;

class Validation
{
    private $validate = [];

    public function validate($post)
    {
        try {
            foreach ($post as $key => $value) {
                $this->validate[$key] = filter_var($value, FILTER_SANITIZE_STRING);
            }
            return (object)$this->validate;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }
}