<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 27/05/2018
 * Time: 00:26
 */

namespace app\utils;


use app\models\Model;
use Closure;
use Exception;
use Throwable;

class Transaction extends Model
{
    public function transactions(Closure $callback)
    {
        $this->connection->beginTransaction();

        try {
            $callback();
            $this->connection->commit();
        } catch (Throwable $e) {
            $this->connection->rollBack();
            $e->getMessage();
        }
    }

    public function model($model)
    {
        return new $model;
    }

    public function __get($name)
    {
        try {
            if (!property_exists($this, $name)) {

                $model = __NAMESPACE__ . '\\' . ucfirst($name);

                return new $model();
            }
        } catch (Exception $e) {
            $e->getMessage();
        }
    }
}