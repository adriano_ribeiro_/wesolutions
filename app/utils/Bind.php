<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 27/05/2018
 * Time: 00:25
 */

namespace app\utils;


class Bind
{
    private static $bind = [];

    public static function bind($key, $value)
    {
        try {
            static::$bind[$key] = $value;
        } catch (\Exception $e) {
            die('Erro na function bind do Bind: ' . $e->getMessage());
        }
    }

    public static function get($key)
    {
        try {
            return static::$bind[$key];
        } catch (\Exception $e) {
            die('Erro na function get do Bind: ' . $e->getMessage());
        }
    }
}