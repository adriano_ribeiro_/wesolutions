<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 27/05/2018
 * Time: 00:23
 */

namespace app\libs;
/*
   *  CORE CONTROLLER CLASS
   *  Loads Models & Views
*/
abstract class Controller
{
    // Simple page redirect
    public function redirect($page)
    {
        header('location: ' . URLROOT . '/' . $page);
    }

    // Lets us load view from controllers
    public function view($url)
    {
        // Check for view file
        if (file_exists('../resources/views/' . $url . '.php')) {
            // Require view file
            require_once '../resources/views/' . $url . '.php';
        } else {
            // No view exists
            echo '<br><h1 align="center">404</h1>';
            die('<h1 align="center">Página não encotrada</h1>');
        }
    }
}