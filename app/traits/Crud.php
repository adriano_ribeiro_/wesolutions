<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 27/05/2018
 * Time: 00:25
 */

namespace app\traits;

use app\dao\DAO;
use PDO;
use PDOException;


trait Crud
{
    public $attributes;
    public $sql;
    public $crud;
    public $stmd;

    public function insert($attributes)
    {
        try {
            $this->attributes = (array)$attributes;
            $email = $_POST['email'];

            $this->sql = DAO::insert($this->table, $this->attributes);

            $this->stmd = $this->connection->prepare($this->sql);

            $validate = $this->connection->prepare("SELECT * FROM {$this->table} WHERE email = ?");

            $validate->execute(array($email));
            if ($validate->rowCount() == 0):
                return $this->stmd->execute($this->attributes);
            else:
                echo "<script> alert('$email já existe'); </script>";
            endif;

        } catch (PDOException $e) {
            die('Erro ao inserir : ' . $e->getMessage());
        }
    }

    public function update($attributes, $where)
    {
        try {
            $this->attributes = (array)$attributes;

            $this->sql = (new DAO)->where($where)->update($this->table, $this->attributes);

            $this->stmd = $this->connection->prepare($this->sql);

            $this->stmd->execute($this->attributes);
            $this->crud = $this->stmd->rowCount();

            return json_encode($this->crud);
        } catch (PDOException $e) {
            die('Erro ao atualizar: ' . $e->getMessage());
        }
    }
}