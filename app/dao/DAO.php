<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 27/05/2018
 * Time: 00:21
 */

namespace app\dao;

use PDOException;


class DAO
{
    private $where;

    public static function insert($table, $attributes)
    {
        try {
            return $sql = "INSERT INTO {$table}(" . implode(',', array_keys($attributes)) . ')
                            VALUES
                            (' . ':' . implode(', :', array_keys($attributes)) . ')';
        } catch (PDOException $e) {
            die('Erro na function insert: ' . $e->getMessage());
        }
    }

    public function where($where)
    {
        $this->where = $where;

        return $this;
    }

    public function update($table, $attributes)
    {
        try {
            $sql = "UPDATE {$table} SET ";

            unset($attributes[array_keys($this->where)[0]]);

            foreach ($attributes as $key => $value) {
                $sql .= "{$key} = :{$key}";
            }

            $sql = rtrim($sql, ', ');

            $where = array_keys($this->where);

            $sql .= " WHERE {$where[0]} = :{$where[0]}";

            return $sql;

        } catch (PDOException $e) {
            die('Erro na function update: ' . $e->getMessage());
        }
    }
}