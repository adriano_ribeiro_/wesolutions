<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 27/05/2018
 * Time: 00:21
 */

namespace app\database;

use PDO;
use PDOException;

class Connection
{
    public static function connect()
    {
        try {
            $config = require "../config/database.php";
            $dsn = "mysql:host={$config['host']};dbname={$config['dbname']};charset={$config['charset']}";
            $options = array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,git
                PDO::ATTR_PERSISTENT => true);

            $pdo = new PDO($dsn, $config['username'], $config['password'], $options);
            //$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            //$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);

            return $pdo;
        } catch (PDOException $exception) {
            die('ERRO na function connect(): ' . $exception->getMessage());
        }
    }
}