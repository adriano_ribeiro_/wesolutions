<?php
/**
 * Created by PhpStorm.
 * User: adria
 * Date: 09/05/2018
 * Time: 15:36
 */
namespace app\services;


use app\utils\Transaction;
use app\utils\Validation;
use PDOException;


class UserService extends JsonService
{
  private $user;
  private $validation;

 public function __construct()
 {
    $this->user = new User();
    $this->validation = new Validation();
}

public function login()
{
    try{
        $this->user->login();
    } catch (PDOException $e){
        $e->getMessage();
    }
}

public function getAllEmail()
{
    try {
//            $this->user->setSql("SELECT {$this->user->table}.email FROM {$this->user->table}");
//            $this->user->setCrud($this->user->getConnection()->query($this->user->getSql()));
//            $this->user->getCrud()->execute();
//
//            return $this->user->getCrud()->fetchAll();

    } catch (PDOException $e) {
        $e->getMessage();
    }
}

public function create()
{
    try {
        $validate = $this->validation->validate($_POST);
        $transaction = new Transaction();

        $transaction->transactions(function () use ($transaction, $validate) {
            $transaction->model(new User)->insert($validate);
        });

    } catch (PDOException $e) {
        die('Erro ao criar usuário: ' . $e->getMessage());
    }
}

public function getAll()
{
    try {
        return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->user->findAll());
    } catch (PDOException $e){
        $e->getMessage();
    }
}

public function getById($id)
{
    try {
        $id1 = $_GET['id'];
        return $this->returnJson($this->codeSuccess, $this->messageSuccess, $this->user->findById($id1, $id));
    } catch (PDOException $e){
        $e->getMessage();
    }
}
}