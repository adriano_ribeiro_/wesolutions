<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 25/06/2018
 * Time: 19:44
 */

namespace app\services;
//header('Content-Type: application/json; charset=UTF-8');


class JsonService
{
    protected $codeSuccess = true;
    protected $codeError = false;
    protected $codeAdv = 2;
    protected $codInfo = 1;
    protected $messageSuccess = 'SUCESSO ';
    protected $messageError = 'ERRO ';
    protected $messageInfo = 'INFORMAÇÃO ';
    protected $messageAdv = 'ADVERTÊNCIA ';
    protected $validationn = 'VALIDAÇÃO ';

    protected function returnJson($code, $message, $data)
    {
        $json = [
            'code' => $code,
            'message' => $message,
            'data' => $data
        ];

        return json_encode($json);
    }
}