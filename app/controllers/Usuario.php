<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 27/05/2018
 * Time: 00:50
 */

use app\libs\Controller;
use app\services\UserService;


class Usuario extends Controller
{
    private $userService;

    public function __construct()
    {
        $this->userService = new UserService();
    }

    public function index()
    {
        $this->view('pages/404');
    }

    public function login()
    {
        try {
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $this->redirect('pages/home');
            } else {
                $this->view('user/login');
            }
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    public function cadastro()
    {
        try {
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $this->userService->create();
                $this->redirect('usuario/login');
            } else {
                $this->view('user/register');
            }
        } catch (Exception $e) {
            die('Exceção na classe Usuario na function cadastro: ' . $e->getMessage());
        }
    }

    public function editar()
    {
        $this->view('user/update');
        try {
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $this->userService->create();
                $this->redirect('usuario/login');
            } else {
                $this->view('user/update');
            }
        } catch (Exception $e) {
            die('Exceção na classe Usuario na function editar: ' . $e->getMessage());
        }
    }

    public function lista()
    {
        try {
            echo $this->userService->getAll();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    public function listaId($id)
    {
        try {
            echo $this->userService->getById($id);
        } catch (Exception $e) {
            $e->getMessage();
        }
    }
}