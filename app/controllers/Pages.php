<?php

use app\libs\Controller;
use app\services\UserService;

class Pages extends Controller
{
    private $userService;

    public function __construct()
    {
        $this->userService = new UserService();
    }

    public function index()
    {
        $this->view('pages/index');
        try {

            $this->userService->login();
        } catch (PDOException $e) {
            $e->getMessage();
        }
    }

    public function home()
    {
        $this->view('pages/home');
    }
}