/** URL'S DE ACESSO **/

var url = {

    user: {
        login: '/wesolutions/usuario/login',
        create: '/wesolutions/usuario/cadastro',
        read: '/wesolutions/home',
        update: '/wesolutions/usuario/editar',
        delete: '/wesolutions/usuario/delete'
    }
}