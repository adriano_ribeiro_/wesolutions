/** FUNÇÕES DO USUÁRIO **/

var user = {

    formLogin: '#user_login',
    formRegister: '#user_register',
    name: '#name',

    index: function() {
        this.salvar();
        this.login();
        $(user.name).focus();
    },

    login: function() {
        $(function(){
            $('#submit_login').click(function(){
                $.post(url.user.login, $('#user_login :input').serializeArray(), function(data){

                });
            });

            $(user.formLogin).submit(function(){
                return false;
            });

        });
    },

    salvar: function() {
        $.post(url.user.create, $(user.formRegister).serialize(), function(data) {
            $(user.formRegister).trigger("reset");
        });
    }
}