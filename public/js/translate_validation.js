 /*
* Translated default messages for the jQuery validation plugin.
* Locale: PT_BR
*/
jQuery.extend(jQuery.validator.messages, {
    required: "CAMPO OBRIGATÓRIO.",
    remote: "POR FAVOR, CORRIJA ESTE CAMPO.",
    email: "POR FAVOR, FORNEÇA UM EMAIL VÁLIDO.",
    url: "POR FAVOR, FORNEÇA UMA URL VÁLIDA.",
    date: "POR FAVOR, FORNEÇA UMA DATA VÁLIDA.",
    dateISO: "POR FAVOR, FORNEÇA UMA DATA (ISO).",
    number: "POR FAVOR, FORNEÇA UM NÚMERO VÁLIDO.",
    digits: "POR FAVOR, FORNEÇA SOMENTE DIGITOS.",
    creditcard: "POR FAVOR, FORNEÇA UM CARACTER DE DIGITO VÁLIDO.",
    equalTo: "POR FAVOR, FORNEÇA O MESMO VALOR NOVAMENTE.",
    accept: "POR FAVOR, FORNEÇA UM VALOR COM UMA extens&atilde;o v&aacute;lida.",
    maxlength: jQuery.validator.format("POR FAVOR, FORNEÇA n&atilde;o mais que {0} caracteres."),
    minlength: jQuery.validator.format("POR FAVOR, FORNEÇA AO MENOS {1} CARACTERES."),
    rangelength: jQuery.validator.format("POR FAVOR, FORNEÇA UM VALOR ENTRE {0} E {1} CARACTERES DE COMPRIMENTO."),
    range: jQuery.validator.format("POR FAVOR, FORNEÇA UM VALOR ENTRE {0} E {1}."),
    max: jQuery.validator.format("POR FAVOR, FORNEÇA UM VALOR MENOR OU IGUAL A {0}."),
    min: jQuery.validator.format("POR FAVOR, FORNEÇA UM VALOR MAIOROUu IGUAL A {0}.")
});