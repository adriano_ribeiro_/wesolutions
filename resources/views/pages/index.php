<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="public/css/bootstrap.min.css" rel="stylesheet">
    <link rel="icon" href="public/image/favicon.ico">
    <title><?php echo SITENAME; ?></title>
</head>
<body>
<div class="container">
    <style>

        body {
            background: #2a3f54 !important;
            margin-top: -9px;
        }

        .wrapper {
            margin-top: 8px;
            margin-bottom: 80px;
        }

        .btn-login {
            background-color: #2a3f54;
        }

        .form-signin {
            max-width: 380px;
            padding: 15px 35px 45px;
            margin: 0 auto;
            background-color: #9acfea;
            border: 1px solid rgba(0, 0, 0, 0.1);

        .form-signin-heading,
        .form-control {
            position: relative;
            font-size: 16px;
            height: auto;
            padding: 10px;
        @include box-sizing(border-box);

        &
        :focus {
            z-index: 2;
        }

        }

        input[type="email"] {
            margin-bottom: -1px;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }

        input[type="password"] {
            margin-bottom: 20px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }

        }
    </style>
    <h1 class="form-signin-heading text-center" style="color: #9acfea">Bar & Restaurante</h1><br>
    <div class="wrapper">
        <form action="<?php echo URLROOT; ?>" method="post" class="form-signin">
            <h1 class="form-signin-heading text-center" style="color: #2a3f54">Login</h1><br>
            <input type="email" class="form-control" id="email" name="email" placeholder="Digite seu email" required
                   autofocus/><br>
            <input type="password" class="form-control" id="password" name="password" placeholder="Digite sua senha"
                   required/><br>
            <button style="color: #9acfea" class="btn btn-lg btn-login btn-block" type="submit">Login</button>
            <br>
            <a style="color: #9acfea" class="btn btn-lg btn-login btn-block"
               href="<?php echo URLROOT; ?>/usuario/cadastro" role="button">Cadastre-se</a>
        </form>
    </div>
    <div class="container text-center">
        <br>
        <p><h6 style="color: #ffffff">copyright &copy; 2018 wesolutions Ltda. Todos os direitos reservados.</h6></p>
    </div>
</div>
</body>
</html>

