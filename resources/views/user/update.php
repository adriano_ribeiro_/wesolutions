<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="../public/css/bootstrap.min.css" rel="stylesheet">
    <link href="../public/css/bootstrap.css" rel="stylesheet">
    <link rel="icon" href="../public/image/favicon.ico">
    <script src="../public/js/jquery.js"></script>
    <script src="../public/js/custom/url.js"></script>
    <script src="../public/js/custom/user.js"></script>
    <title><?php echo SITENAME; ?></title>
</head>
<body>
<br>
<div class="container">
    <style>

        body {
            background: #2a3f54 !important;
            margin-top: -9px;
        }

        .wrapper {
            margin-top: 1px;
            margin-bottom: 1px;
        }

        .btn-custom {
            padding: 1px 15px 3px 2px;
            border-radius: 50px;
        }

        .btn-icon {
            padding: 8px;
            background: #ffffff;
        }

        .buttonCss {
            padding: 10px;
            float: bottom;
            color: #ffffff;
            background-color: #2e6da4;
            margin-top: auto;
        }

        .form-signin {
            max-width: 500px;
            padding: 10px 35px 45px;
            margin: 0 auto;
            background-color: #2a3f54;
            border: 1px solid #ffffff;

        .form-signin-heading,
        .form-control {
            position: relative;
            font-size: 16px;
            height: auto;
            padding: 10px;
        @include box-sizing(border-box);

        &
        :focus {
            z-index: 2;
        }

        }

        input[type="email"] {
            margin-bottom: -1px;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }

        input[type="text"] {
            margin-bottom: -1px;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }

        input[type="password"] {
            margin-bottom: 20px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }

        }
    </style>
    <a href="<?php echo URLROOT; ?>/usuario/login" class="btn btn-primary btn-custom">
        <span class="glyphicon glyphicon-arrow-left img-circle text-primary btn-icon"></span>
        Voltar
    </a>
    <div class="wrapper">
        <form id="user_create" class="form-signin" method="post">
            <h1 class="form-signin-heading text-center" style="color: #ffffff">Atualizar Usuário</h1><br><br>
            <input type="text" class="form-control" id="name" name="name" placeholder="Digite seu nome" required
                   autofocus=""/><br>
            <input type="email" class="form-control" id="email" name="email" placeholder="Digite seu email"
                   required/><br>
            <input type="password" class="form-control" id="password" name="password"
                   placeholder="Digite sua senha"/><br><br>
            <input type="hidden" name="status" value="1">
            <button class="btn btn-lg btn-block buttonCss" type="submit">Cadastre - se</button>
        </form>
    </div>
    <div class="container text-center">
        <br>
        <p><h6 style="color: #ffffff">copyright &copy;2018 wesolutions Ltda. Todos os direitos reservados.</h6></p>
    </div>
</div>
</body>
</html>